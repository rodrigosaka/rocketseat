import { useContext } from 'react';
import { Authcontext } from '../contexts/AuthContext';
import { validadteUserPermissions } from '../utils/validateUserPermissions';

type UseCanParams = {
  permissions?: string[];
  roles?: string[];
};

export function useCan({ permissions, roles }: UseCanParams) {
  const { user, isAuthenticated } = useContext(Authcontext);

  if (!isAuthenticated) return false;

  const userHasValidPermissions = validadteUserPermissions({
    user,
    permissions,
    roles
  });

  return userHasValidPermissions;
}
