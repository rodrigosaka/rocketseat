export type AddProductWishlistProps = {
  onAddWishList: () => void;
  onRequestClose: () => void;
};

export function AddProductWishlist({ onAddWishList, onRequestClose }: AddProductWishlistProps) {
  return (
    <span>
      Deseja adicionar aos favoritos?
      <button onClick={onAddWishList}>Sim</button>
      <button onClick={onRequestClose}>Não</button>
    </span>
  );
}
