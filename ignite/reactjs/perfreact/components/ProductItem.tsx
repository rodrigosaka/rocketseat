import dynamic from 'next/dynamic';
import { memo, useState } from 'react';
import { AddProductWishlistProps } from './AddProductWishlist';
import lodash from 'lodash';

const AddProductWishlist = dynamic<AddProductWishlistProps>(
  () => {
    return import('./AddProductWishlist').then(mod => mod.AddProductWishlist);
  },
  {
    loading: () => <span>Carregando...</span>
  }
);

type ProductItemProps = {
  product: {
    id: number;
    price: number;
    priceFormatted: string;
    title: string;
  };
  onAddWishList: (id: number) => void;
};

function ProductItemComponent({ product, onAddWishList }: ProductItemProps) {
  const [isAddingWishlist, setIsAddingWishlist] = useState(false);

  return (
    <div>
      {product.title} - <strong>{product.priceFormatted}</strong>
      <button onClick={() => setIsAddingWishlist(true)}>Adiconar aos favoritos</button>
      {isAddingWishlist && (
        <AddProductWishlist
          onAddWishList={() => onAddWishList(product.id)}
          onRequestClose={() => setIsAddingWishlist(false)}
        />
      )}
    </div>
  );
}

export const ProductItem = memo(ProductItemComponent, (prevProps, nextProps) => {
  return lodash.isEqual(prevProps.product, nextProps.product);
});
