import type { NextPage } from 'next';
import { FormEvent, useCallback, useState } from 'react';
import { SearchResults } from '../components/SearchResults';

const Home: NextPage = () => {
  const [search, setSearch] = useState('');
  const [results, setResults] = useState<Results>({
    totalPrice: 0,
    data: []
  });

  type Product = {
    id: number;
    price: number;
    priceFormatted: string;
    title: string;
  };

  type Results = {
    totalPrice: number;
    data: Product[];
  };

  async function handleSearch(event: FormEvent) {
    event.preventDefault();

    if (!search.trim()) return;

    const response = await fetch(`http://localhost:3333/products?q=${search}`);
    const data = (await response.json()) as Product[];

    const formatter = new Intl.NumberFormat('pt-BR', {
      style: 'currency',
      currency: 'BRL'
    });

    const product = data.map(product => {
      return {
        id: product.id,
        title: product.title,
        price: product.price,
        priceFormatted: formatter.format(product.price)
      };
    });

    const totalPrice = data.reduce((total: number, producct: Product) => total + producct.price, 0);

    setResults({ totalPrice, data: product });
  }

  const addWishList = useCallback(async (id: number) => {
    console.log(id);
  }, []);

  return (
    <div>
      <h1>Search</h1>

      <form onSubmit={handleSearch}>
        <input type='text' value={search} onChange={e => setSearch(e.target.value)} />
        <button type='submit'>Buscar</button>
      </form>

      <SearchResults
        results={results.data}
        totalPrice={results?.totalPrice}
        onAddWishList={addWishList}
      />
    </div>
  );
};

export default Home;
