import { render, screen, waitFor } from '@testing-library/react';
import { Async } from '.';

it('it renders correctly', async () => {
  render(<Async />);

  expect(screen.getByText('Hello World')).toBeInTheDocument();

  await waitFor(() => {
    expect(screen.queryByText('Button')).not.toBeInTheDocument();
  });
});
