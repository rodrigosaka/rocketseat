import { render, screen } from '@testing-library/react';
import { getSession, useSession } from 'next-auth/client';
import { useRouter } from 'next/router';
import { FaRestroom } from 'react-icons/fa';
import Post, { getStaticProps } from '../../pages/posts/preview/[slug]';
import { getPrismicClient } from '../../services/prismic';

const POST = {
  slug: 'my-new-post',
  title: 'My New Post',
  content: '<p>Post excerpt</p>',
  updatedAt: '10 de Abril'
};

jest.mock('next/router');
jest.mock('next-auth/client');
jest.mock('../../services/prismic');

describe('PostPreview page', () => {
  it('renders correctly', () => {
    const useSessionMocked = useSession as jest.Mock;
    useSessionMocked.mockReturnValueOnce([null, false]);

    render(<Post post={POST} />);

    expect(screen.getByText('My New Post')).toBeInTheDocument();
    expect(screen.getByText('Post excerpt')).toBeInTheDocument();
    expect(screen.getByText('Wanna continue reading?')).toBeInTheDocument();
  });

  it('redirects user to full post when user is subscribed', async () => {
    const useSessionMocked = useSession as jest.Mock;
    const useRouterMocked = useRouter as jest.Mock;
    const pushMock = jest.fn();

    useSessionMocked.mockReturnValueOnce([
      {
        activeSubscription: 'fake-active-subscription'
      },
      false
    ]);

    useRouterMocked.mockReturnValueOnce({
      push: pushMock
    });

    render(<Post post={POST} />);

    expect(pushMock).toHaveBeenCalledWith('/posts/my-new-post');
  });

  it('loads initial data', async () => {
    const getPrismicClientMocked = getPrismicClient as jest.Mock;

    getPrismicClientMocked.mockReturnValueOnce({
      getByUID: jest.fn().mockResolvedValueOnce({
        data: {
          title: [{ type: 'heading', text: 'My new post' }],

          content: [{ type: 'paragraph', text: 'Post content' }]
        },
        last_publication_date: '04-01-2021'
      })
    });

    const response = await getStaticProps({ params: { slug: 'my-new-post' } } as any);

    expect(response).toEqual(
      expect.objectContaining({
        props: {
          post: {
            slug: 'my-new-post',
            title: 'My new post',
            content: '<p>Post content</p>',
            updatedAt: '01 de abril de 2021'
          }
        }
      })
    );
  });
});
